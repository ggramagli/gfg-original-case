package com.afkl.cases.df;

import com.afkl.cases.df.mapping.airport.Airport;
import com.afkl.cases.df.services.AirportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


@Controller
@RequestMapping("/airports")
public class AirportController {

    @Autowired
    private AirportService airportService;

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    List<Airport> findAirport(@RequestParam(value = "term", required = false, defaultValue = "") String searchTerm) {

        if (searchTerm.length() < 3)
            return null;

        Future<List<Airport>> airportByTerm = airportService.findAirportByTerm(searchTerm);
        List<Airport> airports = null;
        try {
            airports = airportByTerm.get(5, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }

        if (airports == null) {
            return null;
        }


        return airports;
    }


}