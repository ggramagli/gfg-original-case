package com.afkl.cases.df;

import com.afkl.cases.df.metrics.CustomMetrics;
import org.springframework.boot.actuate.trace.InMemoryTraceRepository;
import org.springframework.stereotype.Repository;

import java.util.Map;



@Repository
public class MetricsRepository extends InMemoryTraceRepository {


    public MetricsRepository() {
        super.setCapacity(300);
    }

    @Override
    public void add(Map<String, Object> map) {
        super.add(map);
        CustomMetrics.recRequest(Long.parseLong((String)map.get("timeTaken")));

    }
}