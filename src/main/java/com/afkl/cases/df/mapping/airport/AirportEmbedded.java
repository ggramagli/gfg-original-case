package com.afkl.cases.df.mapping.airport;

import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;

import java.util.List;

@JsonSerializableSchema
public class AirportEmbedded {
    public List<Airport> locations;
}
