package com.afkl.cases.df.mapping.metrics;

import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;

@JsonSerializableSchema
public class Metric {

    private final Long totalRequests;
    private final Long minRequestTime;
    private final Long maxRequestTime;
    private final Long averageRequestTime;



    public static class Builder {
        private Long totalRequests;
        private Long minRequestTime;
        private Long maxRequestTime;
        private Long averageRequestTime;

        public Builder() {
            //all mandatory
        }

        public Builder totalRequests(Long totalRequests) {
            this.totalRequests = totalRequests;
            return this;
        }
        public Builder minRequestTime(Long minRequestTime) {
            this.minRequestTime = minRequestTime;
            return this;
        }
        public Builder maxRequestTime(Long maxRequestTime) {
            this.maxRequestTime = maxRequestTime;
            return this;
        }
        public Builder averageRequestTime(Long averageRequestTime) {
            this.averageRequestTime = averageRequestTime;
            return this;
        }

        public Metric build() {
            return new Metric(this);
        }
    }

    private Metric(Builder builder) {
        totalRequests = builder.totalRequests;
        minRequestTime = builder.minRequestTime;
        maxRequestTime = builder.maxRequestTime;
        averageRequestTime = builder.averageRequestTime;
    }

    public Long getTotalRequests() {
        return totalRequests;
    }

    public Long getMinRequestTime() {
        return minRequestTime;
    }

    public Long getMaxRequestTime() {
        return maxRequestTime;
    }

    public Long getAverageRequestTime() {
        return averageRequestTime;
    }
//  public String toString() {
     //   return String.format("{\"totalRequests\": %s \"minRequestTime\": %s \"maxRequestTime\": %s \"average\": %s}",
      //          this.totalRequests, this.minRequestTime, this.maxRequestTime, this.averageRequestTime);
   // }
}
