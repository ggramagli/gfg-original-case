package com.afkl.cases.df.mapping.airport;

import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;

@JsonSerializableSchema
public class Airport {
    public String code;
    public String name;
    public String description;
}
