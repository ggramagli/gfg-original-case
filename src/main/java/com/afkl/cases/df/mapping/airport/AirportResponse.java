package com.afkl.cases.df.mapping.airport;

import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;

@JsonSerializableSchema
public class AirportResponse {
    public AirportEmbedded _embedded;

}
