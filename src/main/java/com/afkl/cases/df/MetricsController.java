package com.afkl.cases.df;

import com.afkl.cases.df.mapping.fares.Fare;

import com.afkl.cases.df.mapping.metrics.Metric;
import com.afkl.cases.df.services.MetricsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.ResponseBody;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


@Controller
@RequestMapping("/custommetrics")
public class MetricsController {

    @Autowired
    private MetricsService metricsService;

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    Metric getCustomMetric() {

        Future<Metric> metricFuture = metricsService.getCustomMetrics();
        Metric metric = null;
        try {
            metric = metricFuture.get(5, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }

        if (metric == null) {
            return null;
        }


        return metric;
    }


}