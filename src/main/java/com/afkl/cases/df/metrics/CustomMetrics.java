package com.afkl.cases.df.metrics;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.LongAccumulator;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.LongBinaryOperator;


@Scope(value="singleton")
@Component
public class CustomMetrics {
    private static LongAdder totalRequests = new LongAdder();
    private static LongBinaryOperator lt = (x, y) -> x<y && x>0?x:y;
    private static LongBinaryOperator gt = (x, y) -> x>y?x:y;
    private static LongBinaryOperator av = (x, y) -> {
        //System.out.println("");
        return y==0?y+x/totalRequests.longValue():(y*(totalRequests.longValue()-1)+x)/totalRequests.longValue();};

    private static LongAccumulator minReq=new LongAccumulator(lt,0L);
    private static LongAccumulator maxReq=new LongAccumulator(gt,0L);
    private static LongAccumulator average=new LongAccumulator(av,0L);


    public static void recRequest(Long ms){
         totalRequests.increment();
         minReq.accumulate(ms);
         maxReq.accumulate(ms);
         average.accumulate(ms);
    }
    public static LongAdder getTotalRequests() {
        return totalRequests;
    }

    public static LongAccumulator getMinReq() {
        return minReq;
    }

    public static LongAccumulator getMaxReq() {
        return maxReq;
    }

    public static LongAccumulator getAverage() {
        return average;
    }
}
