package com.afkl.cases.df.services;

import com.afkl.cases.df.mapping.airport.Airport;
import com.afkl.cases.df.mapping.airport.AirportResponse;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Future;

@Service
public class AirportService extends BaseService {

    private static final String TERM_URL = "%s/airports?term=%s";
    private static final String CODE_URL = "%s/airports/%s";

    @Async
    public Future<List<Airport>> findAirportByTerm(String term) {
        String resourceUrl = String.format(TERM_URL, baseApiUrl, term);
        AirportResponse airportResponse = null;
        try {
            String response = restTemplate.getForObject(resourceUrl, String.class);
            airportResponse = objectMapper.readValue(response, AirportResponse.class);
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return airportResponse != null ? new AsyncResult<>(airportResponse._embedded.locations) : new AsyncResult<>(null);
    }

    public List<Airport> findAirportByCode(String code) {
        String resourceUrl = String.format(CODE_URL, baseApiUrl, code);
        AirportResponse airportResponse = null;
        try {
            String response = restTemplate.getForObject(resourceUrl, String.class);

            airportResponse = objectMapper.readValue(response, AirportResponse.class);
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return airportResponse != null ? airportResponse._embedded.locations : null;
    }
}
