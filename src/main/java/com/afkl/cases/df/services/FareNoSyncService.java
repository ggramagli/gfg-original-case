package com.afkl.cases.df.services;

import com.afkl.cases.df.mapping.fares.Fare;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;


@Component

public class FareNoSyncService extends BaseService implements IFare{

    private static final String FARE_URL = "%s/fares/%s/%s";

    @Override
    public Fare findFare(Object source, Object destination) {
        String resourceUrl = String.format(FARE_URL, baseApiUrl, source, destination);
        Fare fare = null;
        try {
            String response = restTemplate.getForObject(resourceUrl, String.class);
            fare = objectMapper.readValue(response, Fare.class);
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return fare;
    }
}
