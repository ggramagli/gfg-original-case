package com.afkl.cases.df.services;


import com.afkl.cases.df.mapping.airport.Airport;

import java.util.List;

@FunctionalInterface
public interface IAirport {
   Airport findAirportByCode(String code);
}