package com.afkl.cases.df.services;

import com.afkl.cases.df.mapping.fares.Fare;
import com.afkl.cases.df.mapping.metrics.Metric;
import com.afkl.cases.df.metrics.CustomMetrics;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;
import java.util.concurrent.Future;

@Service
public class MetricsService extends BaseService {



    @Async
    public Future<Metric> getCustomMetrics() {

        Metric metric = new Metric.Builder()
                .totalRequests(CustomMetrics.getTotalRequests().longValue())
                .minRequestTime(CustomMetrics.getMinReq().longValue())
                .maxRequestTime(CustomMetrics.getMaxReq().longValue())
                .averageRequestTime(CustomMetrics.getAverage().longValue())
                .build();

        return metric != null ? new AsyncResult<>(metric) : new AsyncResult<>(null);
    }

}
