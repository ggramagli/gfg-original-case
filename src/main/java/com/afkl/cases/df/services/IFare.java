package com.afkl.cases.df.services;

import com.afkl.cases.df.mapping.airport.Airport;
import com.afkl.cases.df.mapping.fares.Fare;

import java.util.List;

@FunctionalInterface
public interface IFare<F, S, R> {
    R findFare(F f, S s);
}