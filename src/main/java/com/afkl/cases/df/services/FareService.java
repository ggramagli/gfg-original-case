package com.afkl.cases.df.services;

import com.afkl.cases.df.mapping.fares.Fare;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

@Service
@EnableAsync
public class FareService extends BaseService {

    private static final String FARE_URL = "%s/fares/%s/%s";

    @Async
    public Future<Fare> findFare(String source, String destination) {
        String resourceUrl = String.format(FARE_URL, baseApiUrl, source, destination);
        Fare fare = null;
        try {
            String response = restTemplate.getForObject(resourceUrl, String.class);
            fare = objectMapper.readValue(response, Fare.class);
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return fare != null ? new AsyncResult<>(fare) : new AsyncResult<>(null);
    }

}
