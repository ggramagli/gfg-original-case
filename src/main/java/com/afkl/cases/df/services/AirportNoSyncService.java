package com.afkl.cases.df.services;

import com.afkl.cases.df.mapping.airport.Airport;
import com.afkl.cases.df.mapping.airport.AirportResponse;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

@Service
public class AirportNoSyncService extends BaseService implements IAirport{


    private static final String CODE_URL = "%s/airports/%s";

    public Airport findAirportByCode(String code) {
        String resourceUrl = String.format(CODE_URL, baseApiUrl, code);
        Airport airportResponse = null;
        try {
            String response = restTemplate.getForObject(resourceUrl, String.class);

            airportResponse = objectMapper.readValue(response, Airport.class);
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return airportResponse != null ? airportResponse: null;
    }
}
