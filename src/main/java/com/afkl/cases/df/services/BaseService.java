package com.afkl.cases.df.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;

public abstract class BaseService {

    @Value("${config.api.baseurl}")
    protected String baseApiUrl;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected OAuth2RestTemplate restTemplate;
}
