package com.afkl.cases.df;


import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.Arrays;

@Configuration
@EnableWebMvc
public class WebConfiguration extends WebMvcConfigurerAdapter {

    @Value("${config.api.baseurl:http://localhost:8080}")
    private String baseUrl;
    @Value("${config.api.authorizationUrl:http://localhost:8080/oauth/authorize}")
    private String authorizeUrl;
    @Value("${config.api.tokenUrl:http://localhost:8080/oauth/token}")
    private String tokenUrl;
    @Value("${config.api.clientId:travel-api-client}")
    private String clientId;
    @Value("${config.api.secret:pwd}")
    private String secret;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/index.html").addResourceLocations("classpath:/static/index.html");
        registry.addResourceHandler("/index-assignment.html").addResourceLocations("classpath:/static/index-assignment.html");
        registry.addResourceHandler("/bower_components/**")
                .addResourceLocations("classpath:/static/bower_components/");
        registry.addResourceHandler("/app/**")
                .addResourceLocations("classpath:/static/app/");


    }

    @Bean
    public OAuth2RestTemplate restTemplate() {
        ClientCredentialsResourceDetails resourceDetails = new ClientCredentialsResourceDetails();
        resourceDetails.setAccessTokenUri(tokenUrl);
        resourceDetails.setClientId(clientId);
        resourceDetails.setClientSecret(secret);
        resourceDetails.setGrantType("client_credentials");
        resourceDetails.setScope(Arrays.asList("read", "write"));

        DefaultOAuth2ClientContext clientContext = new DefaultOAuth2ClientContext();
        return new OAuth2RestTemplate(resourceDetails, clientContext);
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper;
    }

    /*@Primary
    @Bean
    public RemoteTokenServices tokenService() {
        RemoteTokenServices tokenService = new RemoteTokenServices();
        tokenService.setCheckTokenEndpointUrl(
                "http://5.249.149.164:8080/oauth/token");
        tokenService.setClientId("travel-api-client");
        tokenService.setClientSecret("psw");
        return tokenService;
    }*/

}
