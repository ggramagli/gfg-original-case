package com.afkl.cases.df;

import com.afkl.cases.df.mapping.airport.Airport;
import com.afkl.cases.df.mapping.airport.AirportResponse;
import com.afkl.cases.df.mapping.fares.Fare;
import com.afkl.cases.df.mapping.fares.FarePojo;
import com.afkl.cases.df.services.AirportNoSyncService;
import com.afkl.cases.df.services.AirportService;
import com.afkl.cases.df.services.FareNoSyncService;
import com.afkl.cases.df.services.FareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.BiFunction;
import java.util.stream.Stream;

import static java.lang.Integer.parseInt;
import static java.util.concurrent.CompletableFuture.supplyAsync;


@Controller
@RequestMapping("/fare")
public class FareController {

    @Autowired
    private AirportNoSyncService airportService;

    @Autowired
    private FareNoSyncService fareNoSyncService;

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    FarePojo findFare(@RequestParam(value = "c1", defaultValue = "") String source,
                  @RequestParam(value = "c2", defaultValue = "") String destination) {


        return getFarePojo(source,destination).join();
    }

    public CompletableFuture<FarePojo> getFarePojo(String source, String destination) {
        CompletableFuture<Airport> sourceCompletableFuture =
                CompletableFuture.supplyAsync(() -> airportService.findAirportByCode(source));
        CompletableFuture<Airport> destinationCompletableFuture =
                CompletableFuture.supplyAsync(() -> airportService.findAirportByCode(destination));
        CompletableFuture<Fare> fareCompletableFuture =
                CompletableFuture.supplyAsync(() -> fareNoSyncService.findFare(source, destination));
        return CompletableFuture.allOf(sourceCompletableFuture, destinationCompletableFuture, fareCompletableFuture)
                .thenApplyAsync(complete -> {
                    FarePojo result = new FarePojo();

                    Airport lSource = sourceCompletableFuture.join();
                    if (lSource != null) {

                        result.setOrigin( lSource);
                    }

                    Airport lDestination = destinationCompletableFuture.join();
                    if (lDestination != null) {

                        result.setDestination(lDestination);
                    }
                    Fare lFare = fareCompletableFuture.join();
                    if (lFare != null) {
                        result.setAmount(lFare.getAmount());
                        result.setCurrency(lFare.getCurrency());
                    }


                    return result;
                });
    }

}