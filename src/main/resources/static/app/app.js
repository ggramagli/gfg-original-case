(function(angular) {
  angular.module("KLMAPP.controllers", []);
  angular.module("KLMAPP.services", []);
  angular.module("KLMAPP", ["ngResource", "KLMAPP.controllers", "KLMAPP.services"]);
}(angular));