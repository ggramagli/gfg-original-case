(function(angular) {
  var AppController = function($scope, $http) {

     $scope.selected = '';
   

    $scope.getLocation = function(val) {
      return $http.get('airports?term='+val).then(function(response){
        $scope.custommetrics={};
        $scope.standardMetrics={};
        return response.data;
      });
    };

    $scope.getCustomMetrics = function() {
      
      $http.get('custommetrics').then(function(response){
        $scope.customMetrics=response.data;
       
      });
     
       $http.get('metrics').then(function(response){
        var arKeys=Object.keys(response.data);
        $scope.standardMetrics={};
        $scope.standardMetrics.count2x=0;
        $scope.standardMetrics.count4x=0;
        $scope.standardMetrics.count5x=0;
        arKeys.forEach(function(itm){
          if(itm.startsWith('counter.status.2')){
            $scope.standardMetrics.count2x+=(response.data[itm]);
            
          }
          if(itm.startsWith('counter.status.4')){
            $scope.standardMetrics.count4x+=(response.data[itm]);
            
          }
          if(itm.startsWith('counter.status.5')){
            $scope.standardMetrics.count5x+=(response.data[itm]);
           
          }
        });
        
       
      });
    };
   

    $scope.getFare = function(selFcode, selTcode) {
      $scope.loading=true;
      return $http.get('fare', {params: {c1: selFcode.code, c2: selTcode.code}}).then(function(response){
        $scope.fare=response.data;
        $scope.loading=false;
      });
    };

  };
  
  AppController.$inject = ['$scope', '$http'];
  angular.module("KLMAPP.controllers",['ui.bootstrap']).controller("AppController", AppController);
}(angular));